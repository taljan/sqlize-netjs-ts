import { Sequelize } from 'sequelize-typescript';
import { User } from './models/user.entity';
import { ConfigService } from '../shared/config/config.service';

export const databaseProviders = [
    {
        provide: 'SEQUELIZE',
        useFactory: async (configService: ConfigService) => {
            console.log(configService.sequelizeOrmConfig);
            const sequelize = new Sequelize(configService.sequelizeOrmConfig);
            sequelize.addModels([User]);
            await sequelize.sync();
            return sequelize;
        },
        inject: [ConfigService],
    },
];
