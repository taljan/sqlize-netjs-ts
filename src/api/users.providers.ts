import { User } from '../database/models/user.entity';

export const usersProviders = [{ provide: 'UsersRepository', useValue: User }];

