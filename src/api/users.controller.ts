import {UserLoginRequestDto} from './resources/users/dto/user-login-request.dto';
import {Body, Controller, Delete, Get, HttpCode, Post, Put, Req, UseGuards,} from '@nestjs/common';
import {CreateUserDto} from './resources/users/dto/create-user.dto';
import {UsersService} from '../services/resources/users/users.service';
import {UserDto} from './resources/users/dto/user.dto';
import {ApiBearerAuth, ApiOkResponse, ApiTags} from '@nestjs/swagger';
import {UserLoginResponseDto} from './resources/users/dto/user-login-response.dto';
import {AuthGuard} from '@nestjs/passport';
import {UpdateUserDto} from './resources/users/dto/update-user.dto';
import {Role} from './resources/users/auth/role.enum';
import RolesGuard from './resources/users/auth/roles.guard';

@Controller('users')
@ApiTags('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Post('register')
    @ApiOkResponse({ type: UserLoginResponseDto })
    register(
        @Body() createUserDto: CreateUserDto,
    ): Promise<UserLoginResponseDto> {
        return this.usersService.create(createUserDto);
    }

    @Post('login')
    @HttpCode(200)
    @ApiOkResponse({ type: UserLoginResponseDto })
    login(
        @Body() userLoginRequestDto: UserLoginRequestDto,
    ): Promise<UserLoginResponseDto> {
        return this.usersService.login(userLoginRequestDto);
    }

    @Get()
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOkResponse({ type: [UserDto] })
    findAll(): Promise<UserDto[]> {
        return this.usersService.findAll();
    }

    @Get('me')
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOkResponse({ type: UserDto })
    async getUser(@Req() request): Promise<UserDto> {
        return this.usersService.getUser(request.user.id);
    }

    @Put('me')
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOkResponse({ type: UserDto })
    update(
        @Body() updateUserDto: UpdateUserDto,
        @Req() request,
    ): Promise<UserDto> {
        return this.usersService.update(request.user.id, updateUserDto);
    }

    @Delete('me')
    @ApiBearerAuth()
    @UseGuards(RolesGuard(Role.Admin))
    @UseGuards(AuthGuard('jwt'))
    @ApiOkResponse({ type: UserDto })
    delete(@Req() request): Promise<UserDto> {
        return this.usersService.delete(request.user.id);
    }
}
