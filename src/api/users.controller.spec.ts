import {Test} from '@nestjs/testing';
import {UsersController} from './users.controller';
import {UsersService} from '../services/resources/users/users.service';
import {usersProviders} from './users.providers';
import {JwtStrategy} from './resources/users/auth/jwt-strategy';
import {UserDto} from './resources/users/dto/user.dto';
import {Gender} from '../shared/enum/gender';
import {Role} from './resources/users/auth/role.enum';
import {DatabaseModule} from '../database/database.module';

describe('UsersController', () => {
    let usersController: UsersController;
    let usersService: UsersService;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [DatabaseModule],
            controllers: [UsersController],
            providers: [UsersService, ...usersProviders, JwtStrategy]
        }).compile();

        usersService = moduleRef.get(UsersService);
        usersController = moduleRef.get(UsersController);
    });

    describe('findAll', () => {
        it('should return an array of users', async () => {
            const result: UserDto[] = [
                {
                    id: 'user-id-01',
                    firstName: 'testName',
                    lastName: 'testlastName',
                    birthday: '1990-09-18',
                    email: 'test@email.com',
                    gender: Gender.male,
                    roles: Role.User,
                },
            ];
            jest.spyOn(usersService, 'findAll').mockImplementation(async () => result);
            expect(await usersController.findAll()).toBe(result);
        });
    })
})