import { CanActivate, ExecutionContext, mixin, Type } from '@nestjs/common';
import { Role } from './role.enum';
import { Observable } from 'rxjs';

const RolesGuard = (role: Role): Type<CanActivate> => {
    class RoleGuardMixin implements CanActivate {
        canActivate(
            context: ExecutionContext,
        ): boolean | Promise<boolean> | Observable<boolean> {
            const request = context.switchToHttp().getRequest();
            const user = request.user;

            return user?.roles.includes(role);
        }
    }
    return mixin(RoleGuardMixin);
};

export default RolesGuard;
