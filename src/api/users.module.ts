import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { usersProviders } from './users.providers';
import { DatabaseModule } from '../database/database.module';
import { UsersService } from '../services/resources/users/users.service';
import { JwtStrategy } from './resources/users/auth/jwt-strategy';

@Module({
    imports: [DatabaseModule],
    controllers: [UsersController],
    providers: [UsersService, ...usersProviders, JwtStrategy],
    exports: [UsersService],
})
export class UsersModule {}
