import {Module} from '@nestjs/common';
import {UsersModule} from './api/users.module';
import {SharedModule} from './shared/shared.module';
import {ConfigModule} from '@nestjs/config';

@Module({
    imports: [
        ConfigModule.forRoot({isGlobal: true}),
        UsersModule,
        SharedModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
