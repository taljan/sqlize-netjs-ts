FROM node:16.11.0-alpine3.14

WORKDIR /app

COPY package*.json .

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
RUN apk add make
RUN apk add g++

RUN yarn install

COPY . .

EXPOSE 3000

CMD ["yarn", "start:prod"]
